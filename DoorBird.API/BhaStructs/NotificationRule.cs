﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DoorBird.API.BhaStructs {
	public enum EventType {
		Doorbell,
		MotionSensor,
		DoorOpen
	}

	public struct NotificationRule {
		private static readonly TimeSpan DefaultRelaxation = new TimeSpan(0, 0, 0, 10);

		/// <summary>The type of event to notify for.</summary>
		public EventType Event;
			
		/// <summary>HTTP or HTTPS URL to call with GET command if the event occurs.</summary>
		public Uri Callback;

		/// <summary>
		/// Basic or Digest authentication user for the HTTP URL.
		/// Leave blank if not mandatory.
		/// </summary>
		public string Username;

		/// <summary>
		/// Basic or Digest authentication password for the HTTP URL.
		/// Leave blank if not mandatory.
		/// </summary>
		public string Password;

		/// <summary>
		/// 0: disable notification for the given event.
		/// 1: enable notification for the given event.
		/// </summary>
		public bool Subscribe;

		/// <summary>
		/// Relaxation time in seconds for concurrent events.
		/// Default value is 10 seconds.
		/// </summary>
		public TimeSpan Relaxation;

		public NotificationRule(EventType eventType, Uri callback) {
			Event = eventType;
			Callback = callback;
			Username = string.Empty;
			Password = string.Empty;
			Subscribe = true;
			Relaxation = DefaultRelaxation;
		}

		public NotificationRule(EventType eventType, Uri callback, string username = "", string password = "", TimeSpan? relaxation = null, bool subscribe = true) {
			Event = eventType;
			Callback = callback;
			Username = username;
			Password = password;
			Subscribe = subscribe;
			Relaxation = relaxation ?? DefaultRelaxation;
		}

		/// <summary>Decode JSON to a NotificationRule object.</summary>
		/// <param name="json">JSON in BHA format.</param>
		/// <returns>A NotificationRule object.</returns>
		public static NotificationRule Parse(string json) {
			var dict = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(json);
			return Parse(dict);
		}

		/// <summary>Decode a dictionary to a NotificationRule object.</summary>
		/// <param name="dict">Dictionary in BHA format.</param>
		/// <returns>A NotificationRule object.</returns>
		public static NotificationRule Parse(Dictionary<string, dynamic> dict) {
			var eventType = EventTypeExtensions.Inflate(dict["event"]);
			var uri = new Uri(dict["url"]);
			var username = dict["user"] ?? string.Empty;
			var password = dict["password"] ?? string.Empty;
			var subscribe = true;
			var relaxation = DefaultRelaxation;

			if (dict.ContainsKey("subscribe")) {
				subscribe = int.Parse(dict["subscribe"]) == 1;
			}

			if (dict.ContainsKey("relaxation")) {
				relaxation = new TimeSpan(0, 0, 0, int.Parse(dict["relaxation"]));
			}

			return new NotificationRule(eventType, uri, username, password, relaxation, subscribe);
		}

		/// <summary>Encode a NotificationRule object.</summary>
		/// <returns>A dictionary in BHA format.</returns>
		public Dictionary<string, dynamic> ToDictionary() {
			var dict = new Dictionary<string, dynamic>() {
				{ "event", Event.Deflate() },
				{ "url", Uri.EscapeDataString(Callback.ToString()) },
				{ "subscribe", Subscribe ? "1" : "0" },
				{ "relaxation", Relaxation.TotalSeconds }
			};

			if (!string.IsNullOrEmpty(Username)) {
				dict.Add("user", Username);
			}

			if (!string.IsNullOrEmpty(Password)) {
				dict.Add("password", Password);
			}

			return dict;
		}

		/// <summary>Encode a NotificationRule object.</summary>
		/// <returns>JSON in BHA format.</returns>
		public string ToJson() {
			return JsonConvert.SerializeObject(ToDictionary());
		}

		public override string ToString() {
			var active = Subscribe ? "Active" : "Inactive";
			var auth = $"{Username}:{Password}".GetHashCode();
			return $@"[{active} notification on {Event} calls {Callback} as {auth} at interval {Relaxation}]";
		}

		public override int GetHashCode() {
			return ToString().GetHashCode();
		}

		public override bool Equals(object obj) {
			return obj is NotificationRule && GetHashCode() == obj.GetHashCode();
		}

		public static bool operator ==(NotificationRule a, NotificationRule b) {
			return a.Equals(b);
		}

		public static bool operator !=(NotificationRule a, NotificationRule b) {
			return !a.Equals(b);
		}
	}

	internal static class EventTypeExtensions {
		private static readonly Dictionary<string, EventType> Mappings = new Dictionary<string, EventType>() {
			{ "doorbell", EventType.Doorbell },
			{ "dooropen", EventType.DoorOpen },
			{ "motionsensor", EventType.MotionSensor }
		};

		public static string Deflate(this EventType eventType) {
			return eventType.ToString().ToLower();
		}

		public static EventType Inflate(string eventType) {
			if (!Mappings.ContainsKey(eventType)) {
				throw new ArgumentException($"'{eventType}' is not a valid EventType");
			}

			return Mappings[eventType];
		}
	}
}
