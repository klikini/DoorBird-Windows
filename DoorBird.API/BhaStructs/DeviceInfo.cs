﻿using System.Linq;
using System.Net;
using System.Net.NetworkInformation;

namespace DoorBird.API.BhaStructs {
	public struct DeviceInfo {
		public string Firmware { get; private set; }
		public string BuildNumber { get; private set; }
		public PhysicalAddress WifiMacAddress { get; private set; }
		public IPAddress Host { get; private set; }
		public string Username { get; private set; }

		internal DeviceInfo(string firmware, string buildNumber, string wifiMacAddress, IPAddress host, string username) {
			Firmware = firmware;
			BuildNumber = buildNumber;
			WifiMacAddress = PhysicalAddress.Parse(wifiMacAddress);
			Host = host;
			Username = username;
		}

		public override string ToString() {
			return $"[Connection as {Username} to device {WifiMacAddress} at {Host} running firmware {Firmware} build {BuildNumber}]";
		}

		public override int GetHashCode() {
			return ToString().GetHashCode();
		}

		public override bool Equals(object obj) {
			return obj is DeviceInfo && GetHashCode() == obj.GetHashCode();
		}

		public static bool operator ==(DeviceInfo a, DeviceInfo b) {
			return a.Equals(b);
		}

		public static bool operator !=(DeviceInfo a, DeviceInfo b) {
			return !a.Equals(b);
		}
	}

	public static class PhysicalAddressExtensions {
		public static string ToString(this PhysicalAddress address, bool separateBytes = false) {
			if (separateBytes) {
				return string.Join(":", address.GetAddressBytes().Select(b => b.ToString("X2")));
			} else {
				return address.ToString();
			}
		}
	}
}
