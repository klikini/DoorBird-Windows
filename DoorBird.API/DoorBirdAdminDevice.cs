﻿using DoorBird.API.Util;
using System;
using System.Net;
using System.Threading.Tasks;

namespace DoorBird.API {
	public class DoorBirdAdminDevice : DoorBirdDevice {
		/// <summary>Initialize a new DoorBird device.</summary>
		/// <param name="host">The LAN IP address of the device.</param>
		/// <param name="username">A non-administrator username.</param>
		/// <param name="password">The password for the user provided.</param>
		public DoorBirdAdminDevice(IPAddress host, string username, string password) : base(host, username, password) {
			if (!username.EndsWith("0000")) {
				throw new DeviceAccessException(UserType.Administrator);
			}
		}

		public override UserType UserType => UserType.Administrator;

		public override Task<HttpStatusCode> Ready() {
			throw new NotImplementedException();
		}
	}
}
