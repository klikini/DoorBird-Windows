﻿using DoorBird.API.BhaStructs;
using DoorBird.API.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace DoorBird.API {
	public class DoorBirdUserDevice : DoorBirdDevice {
		/// <summary>Initialize a new DoorBird device.</summary>
		/// <param name="host">The LAN IP address of the device.</param>
		/// <param name="username">A non-administrator username.</param>
		/// <param name="password">The password for the user provided.</param>
		public DoorBirdUserDevice(IPAddress host, string username, string password) : base(host, username, password) {
			if (username.EndsWith("0000")) {
				throw new DeviceAccessException(UserType.User);
			}
		}

		public override UserType UserType => UserType.User;

		/// <summary>Test the connection to the device.</summary>
		/// <param name="httpCode">Will hold the HTTP status returned by the device, or 0 if an error occurs.</param>
		/// <returns>Whether the device is reachable.</returns>
		public override async Task<HttpStatusCode> Ready() {
			try {
				var http = new BhaHttpJson(UriTools.Create("info.cgi"));
				await http.Fetch();
				return http.ResponseStatus;
			} catch (UriFormatException ex) {
				Console.Error.WriteLine("Invalid device URI: " + ex.Message);
				return HttpStatusCode.BadRequest;
			} catch (Exception ex) {
				Console.Error.WriteLine("Error checking device status: " + ex.Message);
				return 0;
			}
		}

		/// <summary>A multipart JPEG live video stream with the default resolution and compression as defined in the system configuration.</summary>
		/// <returns>The URL of the stream.</returns>
		public Uri LiveVideoUri => UriTools.Create("video.cgi");

		/// <summary>A JPEG file with the default resolution and compression as defined in the system configuration.</summary>
		/// <returns>The URL of the image.</returns>
		public Uri LiveImageUri => UriTools.Create("image.cgi");

		/// <summary>Energize the door opener/alarm output relay of the device.</summary>
		/// <returns>Whether the call was successful.</returns>
		public async Task<bool> OpenDoor() {
			var http = new BhaHttpJson(UriTools.Create("open-door.cgi"));
			await http.Fetch();
			return http.ReturnCode == 1;
		}

		/// <summary>Energize the light relay of the device.</summary>
		/// <returns>Whether the call was successful.</returns>
		public async Task<bool> TurnLightOn() {
			var http = new BhaHttpJson(UriTools.Create("light-on.cgi"));
			await http.Fetch();
			return http.ReturnCode == 1;
		}

		/// <summary>A past image stored in the cloud.</summary>
		/// <param name="index">Index of the history images, where 1 is the latest and 20 is the oldest.</param>
		/// <returns>The URL of the image.</returns>
		public Uri HistoryImageUri(uint index = 1) => UriTools.Create("history.cgi", new Dictionary<string, dynamic>() {
			{ "index", index }
		});

		/// <summary>Get notification settings.</summary>
		/// <returns>A list of notifications.</returns>
		public async Task<List<NotificationRule>> Notifications() {
			var http = new BhaHttpJson(UriTools.Create("notification.cgi"));
			await http.Fetch();
			var rules = new List<NotificationRule>();

			foreach (Dictionary<string, dynamic> rule in http.ResponseJson) {
				rules.Add(NotificationRule.Parse(rule));
			}

			return rules;
		}

		/// <summary>Reset notification settings.</summary>
		/// <returns>Whether the call was successful.</returns>
		public async Task<bool> ResetNotifications() {
			var http = new BhaHttpJson(UriTools.Create("notification.cgi", new Dictionary<string, dynamic>() {
				{ "reset", 1 }
			}));
			await http.Fetch();
			return http.ReturnCode == 1;
		}

		/// <summary>Add a new notification rule to the device.</summary>
		/// <param name="notification">Notification rule.</param>
		/// <returns>Whether the call was successful.</returns>
		public async Task<bool> SubscribeNotification(NotificationRule notification) {
			var http = new BhaHttpJson(UriTools.Create("notification.cgi", notification.ToDictionary()));
			await http.Fetch();
			return http.ReturnCode == 1;
		}

		/// <summary>Disable an existing notification.</summary>
		/// <param name="eventType">Event type (doorbell, motionsensor, dooropen).</param>
		/// <returns>Whether the call was successful.</returns>
		public async Task<bool> DisableNotification(EventType eventType) {
			var http = new BhaHttpJson(UriTools.Create("notification.cgi", new Dictionary<string, dynamic>() {
				{ "event", eventType.Deflate() },
				{ "subscribe", 0 }
			}));
			await http.Fetch();
			return http.ReturnCode == 1;
		}

		/// <summary>The current state of the doorbell.</summary>
		/// <returns>Whether the doorbell is currently ringing.</returns>
		public async Task<bool> DoorbellState() {
			var http = new BhaHttpMap(UriTools.Create("monitor.cgi", new Dictionary<string, dynamic>() { { "check", "doorbell" } }));
			await http.Fetch();
			return int.Parse(http["doorbell"]) == 1;
		}

		/// <summary>The current state of the motion sensor.</summary>
		/// <returns>Whether motion is currently detected.</returns>
		public async Task<bool> MotionState() {
			var http = new BhaHttpMap(UriTools.Create("monitor.cgi", new Dictionary<string, dynamic>() { { "check", "motionsensor" } }));
			await http.Fetch();
			return int.Parse(http["doorbell"]) == 1;
		}

		/// <summary>Some hardware information about the device.</summary>
		/// <returns>A DeviceInfo object that contains the firmware version, build number, and WiFi MAC address.</returns>
		public async Task<DeviceInfo> Info() {
			var http = new BhaHttpJson(UriTools.Create("info.cgi"));
			await http.Fetch();
			JObject version = http["VERSION"][0];
			return new DeviceInfo(version.Value<string>("FIRMWARE"), version.Value<string>("BUILD_NUMBER"), version.Value<string>("WIFI_MAC_ADDR"), Host, Username);
		}

		/// <summary>Live video URL over RTSP.</summary>
		/// <param name="http">Set to true to use RTSP over HTTP.</param>
		/// <returns>The URL for the MPEG H.264 live video stream.</returns>
		public Uri RtspLiveVideoUrl(bool http = false) => UriTools.Create("mpeg/media.amp", http ? 8557 : 554);

		/// <summary>The HTML5 viewer for interaction on other platforms.</summary>
		/// <returns>The URL of the web viewer.</returns>
		public Uri Html5ViewerUrl => UriTools.Create("view.html");

		/// <summary>Obtain real-time audio (G.711 μ-law) from the device.</summary>
		/// <returns>The URL of an audio stream.</returns>
		public Uri AudioToDeviceUri => UriTools.Create("audio-transmit.cgi");

		/// <summary>Transmit audio (G.711 μ-law) to the device.</summary>
		/// <returns>The URL to which to stream audio data.</returns>
		public Uri AudioFromDeviceUri => UriTools.Create("audio-receive.cgi");
	}
}
