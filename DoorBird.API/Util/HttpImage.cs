﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace DoorBird.API.Util {
	public class HttpImage {
		public Uri Uri { get; private set; }

		public byte[] Image { get; private set; }

		public DateTime ImageTimestamp { get; private set; }

		private string _SaveDirectory;

		public string SaveDirectory {
			get => _SaveDirectory;

			set {
				if (!Directory.Exists(value)) {
					throw new ArgumentException($"Directory {value} does not exist");
				} else {
					_SaveDirectory = value;
				}
			}
		}

		public HttpImage(Uri uri) {
			Uri = uri;
		}

		public async Task<byte[]> Fetch() {
			try {
				var webClient = new WebClient();
				webClient.Headers["Authorization"] = Uri.AuthHeader();
				Image = await webClient.DownloadDataTaskAsync(Uri);

				if (webClient.ResponseHeaders["X-Timestamp"] != null) {
					var seconds = int.Parse(webClient.ResponseHeaders["X-Timestamp"]);
					ImageTimestamp = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds(seconds).ToLocalTime();
				} else {
					ImageTimestamp = DateTime.Now;
				}

				return Image;
			} catch (Exception ex) {
				Console.Error.WriteLine($"Error getting image file from {Uri.PathAndQuery}: {ex.Message}");
				return null;
			}
		}

		public byte[] FetchSync() {
			try {
				var webClient = new WebClient {
					Headers = {["Authorization"] = Uri.AuthHeader()}
				};
				Image = webClient.DownloadData(Uri);

				if (webClient.ResponseHeaders["X-Timestamp"] != null) {
					var seconds = int.Parse(webClient.ResponseHeaders["X-Timestamp"]);
					ImageTimestamp = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds(seconds).ToLocalTime();
				} else {
					ImageTimestamp = DateTime.Now;
				}

				return Image;
			} catch (Exception ex) {
				Console.Error.WriteLine($"Error getting image file from {Uri.PathAndQuery}: {ex.Message}");
				return null;
			}
		}

		public async Task<bool> Save() {
			if (Image == null) {
				throw new NullReferenceException("Refusing to write null image bytes to file");
			}

			string day;

			try {
				var year = Path.Combine(SaveDirectory, ImageTimestamp.Year.ToString());
				var month = Path.Combine(year, ImageTimestamp.Month.ToString().PadLeft(2, '0'));
				day = Path.Combine(month, ImageTimestamp.Day.ToString().PadLeft(2, '0'));

				foreach (var directory in new[] { year, month, day }) {
					if (!Directory.Exists(directory)) {
						Directory.CreateDirectory(directory);
					}
				}
			} catch (Exception ex) {
				Console.Error.WriteLine("Error forming save directory subfolder: " + ex.Message);
				day = ""; // Default to top-level
			}

			var filename = "doorbird_" + ImageTimestamp.ToString("yyyy-MM-dd_HH-mm-ss") + ".jpg";

			try {
				using (var stream = new FileStream(Path.Combine(SaveDirectory, day, filename), FileMode.Create, FileAccess.Write, FileShare.Read, bufferSize: 4096, useAsync: true)) {
					await stream.WriteAsync(Image, 0, Image.Length);
				}

				return true;
			} catch (Exception ex) {
				Console.Error.WriteLine($"Error saving image file {filename}: {ex.Message}");
				return false;
			}
		}
	}
}
