﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace DoorBird.API.Util {
	public class BhaUriTools {
		internal IPAddress Host;
		internal string Username;
		private string Password;

		public BhaUriTools(IPAddress host, string username, string password) {
			Host = host;
			Username = username;
			Password = password;
		}

		public Uri Create(string path, int port = 80) {
			return Create(path, new Dictionary<string, dynamic>(), port);
		}

		public Uri Create(string path, Dictionary<string, dynamic> parameters, int port = 80) {
			var query = parameters.ToQueryString();
			path = $"/bha-api/{path}";

			return new UriBuilder(Uri.UriSchemeHttp, Host.ToString(), port, path, query) {
				UserName = Username,
				Password = Password
			}.Uri;
		}
	}

	static class DictionaryExtensions {
		public static string ToQueryString(this Dictionary<string, dynamic> parameters) {
			if (parameters.Count == 0) {
				return string.Empty;
			}

			var str = new StringBuilder();

			foreach (KeyValuePair<string, dynamic> parameter in parameters) {
				str.Append('&');
				str.Append(parameter.Key);
				str.Append('=');
				str.Append(parameter.Value.ToString());
			}

			return '?' + str.ToString().Substring(1);
		}
	}

	public static class UriExtensions {
		public static string AuthHeader(this Uri uri) {
			var bytes = Encoding.ASCII.GetBytes(uri.UserInfo);
			var b64 = Convert.ToBase64String(bytes);
			return "Basic " + b64;
		}
	}
}
