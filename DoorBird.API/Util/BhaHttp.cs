﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace DoorBird.API.Util {
	class BhaHttp {
		public Uri Uri { get; private set; }

		public string ResponseText { get; private set; }
		public HttpStatusCode ResponseStatus { get; private set; }

		public BhaHttp(Uri uri) {
			Uri = uri;
		}

		public async Task Fetch() {
			try {
				var request = WebRequest.Create(Uri);
				request.Timeout = 30000; // 30 seconds
				request.Headers["Authorization"] = Uri.AuthHeader();

				using (var response = (HttpWebResponse) await request.GetResponseAsync()) {
					ResponseStatus = response.StatusCode;
					using (var stream = response.GetResponseStream())
					using (var reader = new StreamReader(stream)) {
						ResponseText = reader.ReadToEnd();
					}
				}
			} catch (WebException ex) {
				try {
					ResponseStatus = ((HttpWebResponse) ex.Response).StatusCode;
				} catch {
					ResponseStatus = 0;
				}

				Console.Error.WriteLine("Web error with BHA HTTP request: " + ex.Message);
			} catch (SocketException ex) {
				ResponseStatus = HttpStatusCode.RequestTimeout;
				Console.Error.WriteLine("Socket error with BHA HTTP request: " + ex.Message);
			} catch (Exception ex) {
				ResponseStatus = 0;
				Console.Error.WriteLine("Error with BHA HTTP request: " + ex.Message);
			}
		}
	}

	class BhaHttpMap : BhaHttp {
		private Dictionary<string, dynamic> _ResponseMap;

		public BhaHttpMap(Uri uri) : base(uri) { }

		public Dictionary<string, dynamic> ResponseMap {
			get {
				if (ResponseStatus != HttpStatusCode.OK) {
					return null;
				}

				if (_ResponseMap == null) {
					_ResponseMap = ParseMap(ResponseText);
				}

				return _ResponseMap;
			}
		}

		private static Dictionary<string, dynamic> ParseMap(string map) {
			var dictionary = new Dictionary<string, dynamic>();

			foreach (var line in map.Split('\n')) {
				var kv = line.Split('=');
				dictionary.Add(kv[0], kv[1]);
			}

			return dictionary;
		}

		public dynamic this[string key] => ResponseMap[key];
	}

	class BhaHttpJson : BhaHttp {
		private Dictionary<string, dynamic> _ResponseJson;

		public BhaHttpJson(Uri uri) : base(uri) { }

		public dynamic ResponseJson {
			get {
				if (ResponseStatus != HttpStatusCode.OK) {
					return null;
				}

				if (_ResponseJson == null) {
					_ResponseJson = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(ResponseText);
				}

				return _ResponseJson["BHA"];
			}

			set { ; }
		}

		public int ReturnCode {
			get {
				if (ResponseJson == null) {
					ResponseJson = ResponseJson;
				}

				if (ResponseJson != null) {
					return int.Parse((string) ResponseJson["RETURNCODE"]);
				} else {
					return -1;
				}
			}
		}

		public dynamic this[string key] => ResponseJson[key];
	}
}
