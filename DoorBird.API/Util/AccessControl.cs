﻿using System;

namespace DoorBird.API.Util {
	public class DeviceAccessException : Exception {
		public DeviceAccessException(UserType expected)
			: base($"Incorrect device login type; expected {expected}.") { }
	}
}
