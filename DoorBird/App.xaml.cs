﻿using DoorBird.API;
using DoorBird.API.Util;
using DoorBird.Windows;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Threading.Tasks;

namespace DoorBird {
	public partial class App : Application, IDoorBirdClass {
		public DoorBirdDevice Device { get; internal set; }

		internal PushNotificationServer HttpServer;

		#region LiveView

		private LiveView _LiveView;

		public LiveView LiveView {
			get {
				if (!LiveViewExists) {
					_LiveView = new LiveView(new HttpImage(((DoorBirdUserDevice) Device).LiveImageUri));
				}

				return _LiveView;
			}

			internal set {
				_LiveView = value;
			}
		}

		public bool LiveViewExists => _LiveView != null;

		#endregion LiveView
		
		internal async Task<HttpStatusCode> Connect() {
			var settings = DoorBird.Properties.Settings.Default;
			var ip = IPAddress.Parse(settings.IPAddress);
			var device = this.UserDevice(new DoorBirdUserDevice(ip, settings.UserUsername, settings.UserPassword));
			return await device.Ready();
		}
	}

	public interface IDoorBirdClass { }

	internal static class AppExtensions {
		public static Window CurrentWindow(this Page _) => Application.Current.Windows.OfType<Window>()
			.SingleOrDefault(w => w.IsActive);

		public static App DBApp(this IDoorBirdClass _) => (App) Application.Current;
	}
}
