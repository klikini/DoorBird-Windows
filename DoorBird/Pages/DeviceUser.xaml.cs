﻿using DoorBird.API.BhaStructs;
using DoorBird.HistoryImages;
using DoorBird.Properties;
using DoorBird.Windows;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Forms = System.Windows.Forms;

namespace DoorBird.Pages {
	public partial class DeviceUser : Page, IDoorBirdClass {
		public DeviceUser() {
			InitializeComponent();

			var settings = Settings.Default;
			PushNotifEnable.IsChecked = settings.PushHttpServer;
			PushNotifPort.Text = settings.PushPort.ToString();
			PushNotifRingEnable.IsChecked = settings.PushDoorbell;
			PushNotifMotionEnable.IsChecked = settings.PushMotion;
			PushNotifDoorEnable.IsChecked = settings.PushDoorOpen;
			RecordingEnable.IsChecked = settings.RecordEnabled;
			RecordingLocation.Text = settings.RecordFolder;

			IntercomContainer.Content = Intercom = new Intercom();
		}

		#region Buttons

		private void Return_Click(object sender, RoutedEventArgs e) {
			var prompt = MessageBox.Show(Properties.Resources.DisconnectPrompt, string.Empty,
				MessageBoxButton.YesNo, MessageBoxImage.Warning);

			if (prompt != MessageBoxResult.Yes) return;
			
			if (this.DBApp().LiveViewExists) {
				this.DBApp().LiveView.Close();
			}
			
			this.CurrentWindow().Content = new Home(disableAutoconnect: true);
		}

		private void LiveView_Click(object sender, RoutedEventArgs e) {
			var liveView = this.DBApp().LiveView;
			liveView.Show();
			liveView.Focus();
		}

		private void OpenOnline_Click(object sender, RoutedEventArgs e) {
			System.Diagnostics.Process.Start(this.UserDevice().Html5ViewerUrl.ToString());
		}

		private void OpenRings_Click(object sender, RoutedEventArgs e) {
			var cloud = new BhaImageSeriesSource(this.UserDevice());
			new HistoryView(cloud).Show();
		}

		#endregion Buttons

		#region Intercom

		private Intercom Intercom;

		private void Expander_Expanded(object sender, RoutedEventArgs e) {
			Intercom.Connect();
		}

		private void Expander_Collapsed(object sender, RoutedEventArgs e) {
			Intercom.Disconnect();
		}

		#endregion Intercom

		#region Device Information

		private async void DeviceInfo_Expanded(object sender, RoutedEventArgs e) {
			var info = await this.UserDevice().Info();
			DisplayIPAddress.Content = info.Host;
			DisplayUsername.Content = info.Username;
			DisplayFirmwareVersion.Content = info.Firmware;
			DisplayBuildNumber.Content = info.BuildNumber;
			DisplayWifiMacAddress.Content = info.WifiMacAddress.ToString(separateBytes: true);
		}

		#endregion Device Information

		#region Control

		private async void TurnIrOn_Click(object sender, RoutedEventArgs e) {
			if (await this.UserDevice().TurnLightOn()) {
				MessageBox.Show(Properties.Resources.IREnabled, string.Empty,
					MessageBoxButton.OK, MessageBoxImage.Information);
			} else {
				MessageBox.Show(Properties.Resources.IRNotEnabled, string.Empty,
					MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}

		private async void UnlockDoor_Click(object sender, RoutedEventArgs e) {
			if (await this.UserDevice().OpenDoor()) {
				MessageBox.Show(Properties.Resources.DoorOpened, string.Empty,
					MessageBoxButton.OK, MessageBoxImage.Information);
			} else {
				MessageBox.Show(Properties.Resources.DoorNotOpened, string.Empty,
					MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}

		#endregion Control

		#region Recording

		private void RecordingEnable_Checked(object sender, RoutedEventArgs e) {
			Settings.Default.RecordEnabled = true;
			Settings.Default.Save();
		}

		private void RecordingEnable_Unchecked(object sender, RoutedEventArgs e) {
			Settings.Default.RecordEnabled = false;
			Settings.Default.Save();
		}

		private void RecordingLocation_Click(object sender, RoutedEventArgs e) {
			using (var dialog = new Forms.FolderBrowserDialog()) {
				if (dialog.ShowDialog() != Forms.DialogResult.Cancel) {
					Settings.Default.RecordFolder = RecordingLocation.Text = dialog.SelectedPath;
					Settings.Default.Save();
				}
			}
		}

		#endregion Recording

		#region Push Notifications

		#region HTTP Server

		private void PushNotifEnable_Checked(object sender, RoutedEventArgs e) {
			this.DBApp().HttpServer = new PushNotificationServer(Settings.Default.PushPort, out bool ok);

			if (ok) {
				Settings.Default.PushHttpServer = true;
				Settings.Default.Save();
			} else {
				PushNotifEnable.IsChecked = false;
			}
		}

		private void PushNotifEnable_Unchecked(object sender, RoutedEventArgs e) {
			Settings.Default.PushHttpServer = false;
			Settings.Default.Save();

			this.DBApp().HttpServer.Stop();
		}

		#endregion HTTP Server

		#region Port

		private void PushNotifPort_PreviewTextInput(object sender, TextCompositionEventArgs e) {
			var regex = new Regex("[0-9]+");
			e.Handled = !regex.IsMatch(e.Text);
		}

		private void PushNotifPort_TextChanged(object sender, TextChangedEventArgs e) {
			Settings.Default.PushPort = uint.Parse(PushNotifPort.Text);
		}

		private void PushNotifPort_LostFocus(object sender, RoutedEventArgs e) {
			Settings.Default.Save();

			if (this.DBApp().HttpServer?.IsRunning ?? false) {
				var dialog = MessageBox.Show(Properties.Resources.RestartHttpServer, string.Empty,
					MessageBoxButton.YesNo, MessageBoxImage.Question);

				if (dialog == MessageBoxResult.Yes) {
					this.DBApp().HttpServer?.Stop();
					this.DBApp().HttpServer = new PushNotificationServer(Settings.Default.PushPort, out bool ok);
				}
			}
		}

		#endregion Port

		#region Event Types

		private List<NotificationRule> NotificationRules {
			get {
				var host = $"http://{Dns.GetHostName()}:{Settings.Default.PushPort}/?event=";
				var rules = new List<NotificationRule>();

				if (PushNotifRingEnable.IsChecked ?? false) {
					rules.Add(new NotificationRule(EventType.Doorbell,
						new Uri(host + PushNotificationServer.DoorbellEvent)));
				}

				if (PushNotifMotionEnable.IsChecked ?? false) {
					rules.Add(new NotificationRule(EventType.MotionSensor,
						new Uri(host + PushNotificationServer.MotionEvent)));
				}

				if (PushNotifDoorEnable.IsChecked ?? false) {
					rules.Add(new NotificationRule(EventType.DoorOpen,
						new Uri(host + PushNotificationServer.DoorEvent)));
				}

				return rules;
			}
		}

		private void PushNotifRingEnable_Checked(object sender, RoutedEventArgs e) {
			Settings.Default.PushDoorbell = true;
			Settings.Default.Save();
		}

		private void PushNotifRingEnable_Unchecked(object sender, RoutedEventArgs e) {
			Settings.Default.PushDoorbell = false;
			Settings.Default.Save();
		}

		private void PushNotifMotionEnable_Checked(object sender, RoutedEventArgs e) {
			Settings.Default.PushMotion = true;
			Settings.Default.Save();
		}

		private void PushNotifMotionEnable_Unchecked(object sender, RoutedEventArgs e) {
			Settings.Default.PushMotion = false;
			Settings.Default.Save();
		}

		private void PushNotifDoorEnable_Checked(object sender, RoutedEventArgs e) {
			Settings.Default.PushDoorOpen = true;
			Settings.Default.Save();
		}

		private void PushNotifDoorEnable_Unchecked(object sender, RoutedEventArgs e) {
			Settings.Default.PushDoorOpen = false;
			Settings.Default.Save();
		}

		#endregion Event Types

		#region Registration

		private async void PushNotifRegisterBtn_Click(object sender, RoutedEventArgs e) {
			Mouse.OverrideCursor = Cursors.Wait;

			foreach (var rule in NotificationRules) {
				await this.UserDevice().SubscribeNotification(rule);
			}

			Mouse.OverrideCursor = null;
			MessageBox.Show(Properties.Resources.NotificationsSet, string.Empty,
				MessageBoxButton.OK, MessageBoxImage.Information);
		}

		private async void PushNotifClearBtn_Click(object sender, RoutedEventArgs e) {
			Mouse.OverrideCursor = Cursors.Wait;
			await this.UserDevice().ResetNotifications();
			Mouse.OverrideCursor = null;
			MessageBox.Show(Properties.Resources.NotificationsReset, string.Empty,
				MessageBoxButton.OK, MessageBoxImage.Information);
		}

		#endregion Registration

		#endregion Push Notifications
	}
}
