﻿using DoorBird.API.Util;
using NAudio.Codecs;
using NAudio.CoreAudioApi;
using NAudio.Wave;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace DoorBird.Pages {
	public partial class Intercom : Page, IDoorBirdClass {
		private readonly WaveFormat Wave = new WaveFormat(8000, 1);

		public Intercom() {
			InitializeComponent();
		}

		public void Connect() {
			Task.Factory.StartNew(() => {
				try {
					StartDownstream();
				} catch {
					RxConnected.Content = Properties.Resources.Disconnected;
				}
			}, TaskCreationOptions.LongRunning);
			
			Task.Factory.StartNew(() => {
				try {
					StartUpstream();
				} catch {
					TxConnected.Content = Properties.Resources.Disconnected;
				}
			}, TaskCreationOptions.LongRunning);
		}

		public void Disconnect() {
			StopDownstream();
			StopUpstream();
		}

		#region Device -> Computer

		private BufferedWaveProvider DownstreamProvider;
		private IWavePlayer Player;

		private bool DownstreamRequested = false;
		private bool SpeakerMuted = false;
		private MMDevice Speakers;

		private async void StartDownstream() {
			DownstreamRequested = true;

			Speakers = new MMDeviceEnumerator().GetDefaultAudioEndpoint(DataFlow.Render, Role.Console);

			DownstreamProvider = new BufferedWaveProvider(Wave) {
				DiscardOnBufferOverflow = true
			};

			Player = new WaveOut(WaveCallbackInfo.FunctionCallback());
			Player.Init(DownstreamProvider);

			// Establish a connection to the device
			var uri = this.UserDevice().AudioFromDeviceUri;
			var http = new WebClient();
			http.Headers.Add(HttpRequestHeader.Authorization, uri.AuthHeader());

			// Start streaming 
			using (var stream = http.OpenRead(uri)) {
				Application.Current.Dispatcher.Invoke(() => RxConnected.Content = Properties.Resources.Connected);
				Player.Play();

				while (DownstreamRequested) {
					if (SpeakerMuted) {
						Application.Current.Dispatcher.Invoke(() => RxLevel.Value = 0);
						continue;
					}

					double level = Speakers.AudioMeterInformation.MasterPeakValue * 100;
					Application.Current.Dispatcher.Invoke(() => RxLevel.Value = level);

					try {
						var mulawByte = stream.ReadByte();

						if (mulawByte > -1) {
							var wavByte = MuLawDecoder.MuLawToLinearSample((byte) mulawByte);
							DownstreamProvider.AddSamples(new [] { (byte) wavByte }, 0, 1);
						}
					} catch {
						// Stream disconnected, free up resources
						Application.Current.Dispatcher.Invoke(() => RxConnected.Content = Properties.Resources.ConnectingDeferred);
						break;
					}
				}

				stream.Close();
			}

			// Wait and try to reconnect
			if (DownstreamRequested) {
				await Task.Delay(100);
				Application.Current.Dispatcher.Invoke(() => RxConnected.Content = Properties.Resources.Connecting);
				StartDownstream();
			}
		}

		private void StopDownstream() {
			DownstreamRequested = false;

			Player?.Stop();
			Player?.Dispose();
		}

		private void MuteSpeaker_Checked(object sender, RoutedEventArgs e) {
			SpeakerMuted = true;
		}

		private void MuteSpeaker_Unchecked(object sender, RoutedEventArgs e) {
			SpeakerMuted = false;
		}

		#endregion

		#region Computer -> Device

		private WaveInEvent MicrophoneStream;
		private MMDevice Microphone;
		private Queue<byte[]> MicBuffer;

		private bool UpstreamRequested = false;
		private bool MicMuted = false;

		private async void StartUpstream() {
			UpstreamRequested = true;

			Microphone = new MMDeviceEnumerator().GetDefaultAudioEndpoint(DataFlow.Capture, Role.Console);

			MicBuffer = new Queue<byte[]>();

			MicrophoneStream = new WaveInEvent() {
				WaveFormat = Wave
			};
			MicrophoneStream.DataAvailable += WaveSource_DataAvailable;
			MicrophoneStream.StartRecording();

			// Establish a connection to the device
			var uri = this.UserDevice().AudioToDeviceUri;
			var http = new WebClient();
			http.Headers.Add(HttpRequestHeader.Authorization, uri.AuthHeader());
			http.Headers.Add(HttpRequestHeader.ContentType, "audio/basic");
			http.Headers.Add(HttpRequestHeader.CacheControl, "no-cache");

			// Start streaming
			try {
				using (var stream = http.OpenWrite(uri)) {
					Application.Current.Dispatcher.Invoke(() => TxConnected.Content = Properties.Resources.Connected);

					while (UpstreamRequested) {
						if (MicMuted) {
							Application.Current.Dispatcher.Invoke(() => TxLevel.Value = 0);
							continue;
						}

						double level = Microphone.AudioMeterInformation.MasterPeakValue * 100;
						Application.Current.Dispatcher.Invoke(() => TxLevel.Value = level);

						try {
							if (MicBuffer.Count > 0) {
								var frame = MicBuffer.Dequeue().Select(b => MuLawEncoder.LinearToMuLawSample(b)).ToArray();
								stream.Write(frame, 0, frame.Length);
							}
						} catch {
							// Stream disconnected, free up resources
							Application.Current.Dispatcher.Invoke(() => TxConnected.Content = Properties.Resources.ConnectingDeferred);
							break;
						}
					}

					// Wait and try to reconnect
					if (UpstreamRequested) {
						await Task.Delay(100);
						Application.Current.Dispatcher.Invoke(() => TxConnected.Content = Properties.Resources.Connecting);
						StartUpstream();
					}

					stream.Flush();
					stream.Close();
				}
			} catch (System.Exception ex) {
				System.Diagnostics.Debug.WriteLine(ex.Message);
			}
		}

		private void WaveSource_DataAvailable(object sender, WaveInEventArgs e) {
			MicBuffer.Enqueue(e.Buffer);
		}

		private void StopUpstream() {
			UpstreamRequested = false;

			MicrophoneStream?.Dispose();
		}

		private void MuteMic_Checked(object sender, RoutedEventArgs e) {
			MicMuted = true;
		}

		private void MuteMic_Unchecked(object sender, RoutedEventArgs e) {
			MicMuted = false;
		}

		#endregion
	}
}
