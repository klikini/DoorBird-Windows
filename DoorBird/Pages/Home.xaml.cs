﻿using System;
using System.Linq;
using DoorBird.HistoryImages;
using DoorBird.Windows;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DoorBird.API;
using DoorBird.Properties;
using Forms = System.Windows.Forms;

namespace DoorBird.Pages {
	public partial class Home : Page, IDoorBirdClass {
		public Home() {
			InitializeComponent();
			var settings = LoadSettings();
			
			if (!string.IsNullOrEmpty(settings.IPAddress) &&
			    !string.IsNullOrEmpty(settings.UserUsername) &&
			    !string.IsNullOrEmpty(settings.UserPassword))
			{
				Connect();
			}
		}

		public Home(bool disableAutoconnect = true) {
			InitializeComponent();
			LoadSettings();
		}

		private Settings LoadSettings() {
			ToggleLoginForm(enabled: false);
			var settings = Properties.Settings.Default;
			LoginIpEntry.Text = settings.IPAddress;
			LoginUsernameEntry.Text = settings.UserUsername;
			LoginPasswordEntry.Password = settings.UserPassword;
			ToggleLoginForm(enabled: true);
			return settings;
		}

		private async void ConnectButton_Click(object sender, RoutedEventArgs e) {
			ToggleLoginForm(false);
			Mouse.OverrideCursor = Cursors.Wait;

			// Save entries

			var settings = Properties.Settings.Default;
			settings.IPAddress = LoginIpEntry.Text;
			settings.UserUsername = LoginUsernameEntry.Text;
			settings.UserPassword = LoginPasswordEntry.Password;

			if (SaveLoginToggle.IsChecked ?? false) {
				settings.Save();
			}

			// Connect to device
			Connect();
		}

		private async void Connect() {
			var status = await this.DBApp().Connect();
			
			Mouse.OverrideCursor = null;
			ToggleLoginForm(true);

			if (status != HttpStatusCode.OK) {
				MessageBox.Show(Properties.Resources.ConnectionError, "HTTP " + status,
					MessageBoxButton.OK, MessageBoxImage.Error);
				return;
			}

			if (Environment.GetCommandLineArgs().Contains("/live")) {
				var settings = Properties.Settings.Default;
				var device = this.UserDevice(new DoorBirdUserDevice(IPAddress.Parse(settings.IPAddress),
					settings.UserUsername, settings.UserPassword));
				try {
					this.DBApp().LiveView.Show();
					((Window) Parent).Close();
				} catch (Exception ex) {
					MessageBox.Show("Invalid device user login.", "Error opening live view",
						MessageBoxButton.OK, MessageBoxImage.Error);
				}
			} else {
				this.CurrentWindow().Content = new DeviceUser();
			}
		}

		private void ToggleLoginForm(bool enabled) {
			foreach (var control in new Control[] {
				LoginIpEntry,
				LoginUsernameEntry,
				LoginPasswordEntry,
				SaveLoginToggle
			}) {
				control.IsEnabled = enabled;
			}
		}

		private void OpenHistory_Click(object sender, RoutedEventArgs e) {
			using (var dialog = new Forms.FolderBrowserDialog()) {
				if (dialog.ShowDialog() == Forms.DialogResult.Cancel) return;
				
				var files = new SavedImageSeriesSource(dialog.SelectedPath);
				if (files.Count > 0) {
					new HistoryView(files, true).Show();
				} else {
					MessageBox.Show(Properties.Resources.NoSavedImages, string.Empty,
						MessageBoxButton.OK, MessageBoxImage.Warning);
				}
			}
		}
	}
}
