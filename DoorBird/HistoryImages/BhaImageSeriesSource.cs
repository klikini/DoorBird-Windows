﻿using DoorBird.API;
using DoorBird.API.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Windows.Media.Imaging;

namespace DoorBird.HistoryImages {
	internal class BhaImageSeriesSource : IReadOnlyList<HistoryImageSource> {
		private class BhaCachedImage {
			public HistoryImageSource ImageSource { get; private set; }
			public DateTime Cached { get; private set; }

			public BhaCachedImage(HistoryImageSource imageSource) {
				ImageSource = imageSource;
				Cached = DateTime.Now;
			}

			public bool Expired => Cached.AddMinutes(1) > DateTime.Now;
		}

		private Dictionary<int, BhaCachedImage> Cache;
		private DoorBirdUserDevice Device;

		public BhaImageSeriesSource(DoorBirdUserDevice device) {
			Cache = new Dictionary<int, BhaCachedImage>();
			Device = device;
		}

		public int Count => 20;

		public HistoryImageSource this[int index] {
			get {
				if (index < 0 || index > Count - 1) {
					throw new ArgumentOutOfRangeException($"Cloud image {index + 1} will not exist");
				}

				if (!Cache.ContainsKey(index) || !Cache[index].Expired) {
					var uri = Device.HistoryImageUri((uint) (index + 1));
					var http = new HttpImage(uri);
					http.FetchSync();

					if (http.Image == null) {
						return null;
					}

					using (var stream = new MemoryStream(http.Image)) {
						var imageSource = BitmapFrame.Create(stream, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
						Cache[index] = new BhaCachedImage(new HistoryImageSource(imageSource, http.ImageTimestamp));
					}
				}

				return Cache[index].ImageSource;
			}
		}

		public IEnumerator<HistoryImageSource> GetEnumerator() {
			for (var i = 0; i < Count; i++) {
				yield return this[i];
			}
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return GetEnumerator();
		}
	}
}
