﻿using System;
using System.Windows.Media;

namespace DoorBird.HistoryImages {
	internal class HistoryImageSource {
		public ImageSource ImageSource { get; }
		public DateTime Timestamp { get; }

		public HistoryImageSource(ImageSource imageSource, DateTime timestamp) {
			ImageSource = imageSource;
			Timestamp = timestamp;
		}
	}
}
