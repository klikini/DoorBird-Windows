﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows.Media.Imaging;

namespace DoorBird.HistoryImages {
	internal class SavedImageSeriesSource : IReadOnlyList<HistoryImageSource> {
		private IReadOnlyList<string> Filenames;

		public SavedImageSeriesSource(IList<string> filenames) {
			Filenames = new ReadOnlyCollection<string>(filenames);
		}

		public SavedImageSeriesSource(string folderPath, bool recursive = true) {
			if (!Directory.Exists(folderPath)) {
				throw new ArgumentException("Directory not found: " + folderPath);
			}

			var filenames = new List<string>();
			AddFiles(folderPath, ref filenames, recursive);
			Filenames = new ReadOnlyCollection<string>(filenames);
		}

		private void AddFiles(string folderPath, ref List<string> filenames, bool recursive) {
			filenames.AddRange(Directory.GetFiles(folderPath));

			if (recursive) {
				foreach (var directory in Directory.GetDirectories(folderPath)) {
					AddFiles(directory, ref filenames, recursive);
				}
			}
		}

		public int Count => Filenames.Count;

		public HistoryImageSource this[int index] {
			get {
				var filename = Filenames[index];
				var imageSource = new BitmapImage(new Uri(filename));

				DateTime timestamp;

				try {
					var dateText = filename.Replace(".jpg", "");
					var parts = dateText.Split('_');
					var date = parts[1].Split('-');
					var time = parts[2].Split('-');
					timestamp = new DateTime(int.Parse(date[0]), int.Parse(date[1]), int.Parse(date[2]), int.Parse(time[0]), int.Parse(time[1]), int.Parse(time[2]));
				} catch {
					timestamp = File.GetCreationTime(filename);
				}

				return new HistoryImageSource(imageSource, timestamp);
			}
		}

		public IEnumerator<HistoryImageSource> GetEnumerator() {
			for (var i = 0; i < Filenames.Count; i++) {
				yield return this[i];
			}
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return GetEnumerator();
		}
	}
}
