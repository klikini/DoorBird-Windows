﻿using DoorBird.Properties;
using System;
using System.Net;
using System.Threading.Tasks;
using System.Windows;
using NetFwTypeLib;

namespace DoorBird {
	class PushNotificationServer : IDoorBirdClass, IDisposable {
		public const string DoorbellEvent = "doorbell";
		public const string MotionEvent = "motionsensor";
		public const string DoorEvent = "dooropen";

		private readonly HttpListener Listener;

		public PushNotificationServer(uint port, out bool ok) {
			Listener = new HttpListener();
			Listener.Prefixes.Add($"http://localhost:{port}/");

			try {
				Listener.Start();
				//ConfigureFirewall(port);
				Task.Factory.StartNew(() => Run(), TaskCreationOptions.LongRunning);
				ok = true;
			} catch (UnauthorizedAccessException ex) {
				System.Diagnostics.Debug.WriteLine(ex.Message);
				ok = true; // Maybe
				MessageBox.Show(Resources.FirewallError, ex.Message, MessageBoxButton.OK, MessageBoxImage.Error);
			} catch (Exception ex) {
				System.Diagnostics.Debug.WriteLine(ex.Message);
				ok = false;
				MessageBox.Show(Resources.HttpServerError, ex.Message, MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}

		public void Stop() {
			try {
				Listener.Stop();
			} catch { }
		}

		public bool IsRunning => Listener.IsListening;

		private async void Run() {
			while (Listener.IsListening) {
				try {
					// Wait for connection
					var context = await Listener.GetContextAsync();

					// Read request
					HttpStatusCode status = HttpStatusCode.OK;
					switch (context.Request.QueryString["event"]) {
						case DoorbellEvent:
							RaiseNotification(Resources.PushDoorbell);
							break;
						case MotionEvent:
							RaiseNotification(Resources.PushMotion);
							break;
						case DoorEvent:
							RaiseNotification(Resources.PushDoorOpen);
							break;
						default:
							status = HttpStatusCode.BadRequest;
							break;
					}

					// Send response
					context.Response.StatusCode = (int) status;
					context.Response.SendChunked = true;
					context.Response.Close();
				} catch { }
			}		
		}

		private void RaiseNotification(string text) {
			Task.Run(() => {
				var dialog = MessageBox.Show(text, Resources.PushTitle, MessageBoxButton.YesNo, MessageBoxImage.Information);

				if (dialog == MessageBoxResult.Yes) {
					Application.Current.Dispatcher.Invoke(delegate {
						var liveView = this.DBApp().LiveView;
						liveView.Show();
						liveView.Focus();
					});
				}
			});
		}

		private static void ConfigureFirewall(uint port) {
			var policyType = Type.GetTypeFromProgID("HnetCfg.FwPolicy2");
			var ruleType = Type.GetTypeFromProgID("HNetCfg.FWRule");

			var policy = (INetFwPolicy2) Activator.CreateInstance(policyType);
			var profiles = policy.CurrentProfileTypes;

			foreach (INetFwRule2 existingRule in policy.Rules) {
				if (existingRule.LocalPorts?.Contains(port.ToString()) ?? false) {
					return;
				}
			}

			var rule = (INetFwRule2) Activator.CreateInstance(ruleType);
			rule.Enabled = true;
			rule.Action = NET_FW_ACTION_.NET_FW_ACTION_ALLOW;
			rule.Protocol = (int) NET_FW_IP_PROTOCOL_.NET_FW_IP_PROTOCOL_TCP;
			rule.LocalPorts = port.ToString();
			rule.Name = "DoorBird push notifications on port " + port;
			rule.Profiles = profiles;

			policy.Rules.Add(rule);
		}

		public void Dispose() {
			((IDisposable) Listener).Dispose();
		}
	}
}
