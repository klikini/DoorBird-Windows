﻿using DoorBird.API;
using DoorBird.API.Util;
using System.Windows;

namespace DoorBird {
	public static class DeviceAccessExtensions {
		private static DoorBirdDevice _Device {
			get {
				return ((App) Application.Current).Device;
			}

			set {
				((App) Application.Current).Device = value;
			}
		}

		private static DoorBirdAdminDevice _AdminDevice {
			get {
				if (_Device.UserType == UserType.Administrator) {
					return (DoorBirdAdminDevice) _Device;
				} else {
					throw new DeviceAccessException(UserType.Administrator);
				}
			}

			set {
				if (value.UserType != UserType.Administrator) {
					throw new DeviceAccessException(UserType.Administrator);
				} else {
					_Device = value;
				}
			}
		}

		private static DoorBirdUserDevice _UserDevice {
			get {
				if (_Device.UserType == UserType.User) {
					return (DoorBirdUserDevice) _Device;
				} else {
					throw new DeviceAccessException(UserType.Administrator);
				}
			}

			set {
				if (value.UserType != UserType.User) {
					throw new DeviceAccessException(UserType.User);
				} else {
					_Device = value;
				}
			}
		}

		public static DoorBirdDevice Device(this IDoorBirdClass _) => _Device;
		public static DoorBirdDevice Device(this IDoorBirdClass _, DoorBirdDevice value) => _Device = value;

		public static DoorBirdUserDevice UserDevice(this IDoorBirdClass _) => _UserDevice;
		public static DoorBirdUserDevice UserDevice(this IDoorBirdClass _, DoorBirdUserDevice value) => _UserDevice = value;

		public static DoorBirdAdminDevice AdminDevice(this IDoorBirdClass _) => _AdminDevice;
		public static DoorBirdAdminDevice AdminDevice(this IDoorBirdClass _, DoorBirdAdminDevice value) => _AdminDevice = value;
	}
}
