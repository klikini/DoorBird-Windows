﻿using DoorBird.API.Util;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Forms = System.Windows.Forms;

namespace DoorBird.Windows {
	public partial class LiveView : Window, IDoorBirdClass {
		public HttpImage HttpImage { get; set; }

		public CancellationTokenSource CancellationToken { get; private set; }

		public LiveView(HttpImage image) {
			InitializeComponent();
			AlwaysOnTop.IsChecked = Topmost = Properties.Settings.Default.LiveViewOnTop;
			HttpImage = image;

			CancellationToken = new CancellationTokenSource();
			Task.Run(async () => {
				do {
					try {
						await Application.Current.Dispatcher.InvokeAsync(Update);
						await Task.Delay((int) Properties.Settings.Default.FrameInterval, CancellationToken.Token);
					} catch {
						break;
					}
				} while (true);
			});
		}

		private async Task Update() {
			if (HttpImage == null) {
				return;
			}

			await HttpImage.Fetch();
			if (HttpImage.Image == null) {
				return;
			}

			using (var stream = new MemoryStream(HttpImage.Image)) {
				Image.Source = BitmapFrame.Create(stream, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
			}
		}

		private async void Save_Click(object sender, RoutedEventArgs e) {
			using (var dialog = new Forms.FolderBrowserDialog()) {
				if (dialog.ShowDialog() == Forms.DialogResult.Cancel) {
					return;
				}

				HttpImage.SaveDirectory = dialog.SelectedPath;
				QuickSaveButton.IsEnabled = true;
			}

			await HttpImage.Save();
		}

		private async void QuickSave_Click(object sender, RoutedEventArgs e) {
			await HttpImage.Save();
		}

		private async void UnlockDoor_Click(object sender, RoutedEventArgs e) {
			if (await this.UserDevice().OpenDoor()) {
				MessageBox.Show(Properties.Resources.DoorOpened, string.Empty, MessageBoxButton.OK, MessageBoxImage.Information);
			} else {
				MessageBox.Show(Properties.Resources.DoorNotOpened, string.Empty, MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}

		private async void TurnIrOn_Click(object sender, RoutedEventArgs e) {
			if (await this.UserDevice().TurnLightOn()) {
				MessageBox.Show(Properties.Resources.IREnabled, string.Empty, MessageBoxButton.OK, MessageBoxImage.Information);
			} else {
				MessageBox.Show(Properties.Resources.IRNotEnabled, string.Empty, MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}

		private void Close_Click(object sender, RoutedEventArgs e) {
			Close();
		}

		private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
			CancellationToken.Cancel();
			this.DBApp().LiveView = null;
		}

		private void Window_KeyDown(object sender, KeyEventArgs e) {
			if ((Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)) && e.Key == Key.S) {
				(QuickSaveButton.IsEnabled ? (Action<object, RoutedEventArgs>) QuickSave_Click : Save_Click)(sender, e);
			}
		}

		private void Image_MouseDown(object sender, MouseButtonEventArgs e) {
			if (e.ChangedButton == MouseButton.Left) {
				DragMove();
			}
		}

		private void AlwaysOnTop_Checked(object sender, RoutedEventArgs e) {
			Topmost = true;

			// Save
			var settings = Properties.Settings.Default;
			settings.LiveViewOnTop = true;
			settings.Save();
		}

		private void AlwaysOnTop_Unchecked(object sender, RoutedEventArgs e) {
			Topmost = false;

			// Save
			var settings = Properties.Settings.Default;
			settings.LiveViewOnTop = false;
			settings.Save();
		}
	}
}
