﻿using DoorBird.HistoryImages;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace DoorBird.Windows {
	public partial class HistoryView : Window {
		internal IReadOnlyList<HistoryImageSource> Images { get; private set; }
		private uint CurrentIndex;
		private bool Slideshow, Playing = false;
		private DispatcherTimer Timer;

		internal HistoryView(IReadOnlyList<HistoryImageSource> images, bool slideshow = false) {
			Images = images;
			Slideshow = slideshow;

			if (Images.Count == 0) {
				Close();
			} else {
				CurrentIndex = 0;
				InitializeComponent();
				Update();

				PlayBtn.IsEnabled = Slideshow;

				Timer = new DispatcherTimer() {
					Interval = new TimeSpan(0, 0, 0, 0, milliseconds: (int) Properties.Settings.Default.FrameInterval),
				};
				Timer.Tick += new EventHandler((obj, e) => Next());
			}
		}

		private HistoryImageSource CurrentImage => Images[(int) CurrentIndex];

		private void Update() {
			Mouse.OverrideCursor = Cursors.Wait;

			// Update nav buttons
			if (Playing) {
				PrevBtn.IsEnabled = NextBtn.IsEnabled = false;				
			} else {
				PrevBtn.IsEnabled = CurrentIndex > 0;
				NextBtn.IsEnabled = CurrentIndex + 1 < Images.Count;
			}

			// Update play/pause button
			PlayIcon.Visibility = Playing ? Visibility.Hidden : Visibility.Visible;
			PauseIcon.Visibility = Playing ? Visibility.Visible : Visibility.Hidden;

			// Update image
			if (CurrentImage?.ImageSource != null) {
				ImageView.Source = CurrentImage.ImageSource;
			} else {
				NextBtn.IsEnabled = false;
				MessageBox.Show(Properties.Resources.EndSavedImages, string.Empty, MessageBoxButton.OK, MessageBoxImage.Exclamation);
				Previous();
				return;
			}

			// Update text
			FilenameDisplay.Text = $"{CurrentIndex + 1}/{Images.Count}: {CurrentImage.Timestamp.ToLongDateString()} {CurrentImage.Timestamp.ToLongTimeString()}";

			Mouse.OverrideCursor = null;
		}

		private void Previous(uint deltaIndex = 1) {
			if ((int) CurrentIndex - deltaIndex >= 0) {
				CurrentIndex -= deltaIndex;
			}

			Update();
		}

		private void Next(uint deltaIndex = 1) {
			if (CurrentIndex + deltaIndex < Images.Count) {
				CurrentIndex += deltaIndex;
			} else if (Playing) {
				CurrentIndex = 0;
			}

			Update();
		}

		private void Index(uint index) {
			CurrentIndex = index;
			Update();
		}

		private void PrevBtn_Click(object sender, RoutedEventArgs e) {
			Previous();
		}

		private void NextBtn_Click(object sender, RoutedEventArgs e) {
			Next();
		}

		private void PlayBtn_Click(object sender, RoutedEventArgs e) {
			if (PlayIcon.Visibility == Visibility.Visible) {
				// Play
				Playing = true;
				Update();
				Timer.Start();
			} else if (PauseIcon.Visibility == Visibility.Visible) {
				// Pause
				Playing = false;
				Update();
				Timer.Stop();
			}
		}

		private void Window_KeyDown(object sender, KeyEventArgs e) {
			switch (e.Key) {
				case Key.Left:
					Previous();
					break;
				case Key.Right:
					Next();
					break;
				case Key.Home:
					Index(0);
					break;
				case Key.End:
					Index((uint) Images.Count - 1);
					break;
				case Key.PageUp:
					Previous(2u);
					break;
				case Key.PageDown:
					Next(2u);
					break;
				default:
					return;
			}

			e.Handled = true;
		}
	}
}
