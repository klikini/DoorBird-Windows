﻿using DoorBird.API;
using DoorBird.API.Util;
using DoorBird.Pages;
using System;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;

namespace DoorBird.Windows {
	public partial class MainWindow : Window, IDoorBirdClass, IDisposable {
		private Timer Timer;
		private string RecordFolder;

		public MainWindow() {
			InitializeComponent();
			Content = new Home();

			// Start recording
			RecordFolder = Properties.Settings.Default.RecordFolder;
			Timer = new Timer(1000);
			Timer.Elapsed += (source, e) => Task.Run(() => Record());
			Timer.Enabled = true;
		}

		public async Task<bool> Record() {
			if (!Properties.Settings.Default.RecordEnabled) {
				return false;
			}

			if (this.Device() == null || this.Device().UserType != UserType.User) {
				return false;
			}

			var http = new HttpImage(this.UserDevice().LiveImageUri) {
				SaveDirectory = RecordFolder
			};

			await http.Fetch();

			if (http.Image != null) {
				try {
					await http.Save();
				} catch {
					MessageBox.Show(Properties.Resources.InvalidRecordLocation, string.Empty, MessageBoxButton.OK, MessageBoxImage.Error);
					Properties.Settings.Default.RecordEnabled = false;
					Properties.Settings.Default.Save();
				}
				return true;
			} else {
				return false;
			}
		}

		public void Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool managed) {
			if (managed) {
				((IDisposable) Timer).Dispose();
			}
		}
	}
}
